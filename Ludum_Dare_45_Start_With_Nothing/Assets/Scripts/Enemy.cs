﻿using Game.Manager;
using Game.Nothing;
using UnityEngine;

namespace Game.Enemis
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] float speed;
        private float xOffset = 0;
        [SerializeField] float yOffset = -1f;
        [SerializeField] GameObject enemyDeathPrefab;

        void Update()
        {
            transform.position += new Vector3(xOffset, yOffset, 0) * speed * Time.deltaTime;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Nothing")
            {
                if (!collision.GetComponent<NothingObject>().HasShooted) { return; }
                DestroySequence();
            }

            if (collision.gameObject.tag == "BottomTriggerBorder")
            {
                GameManager gameManager = FindObjectOfType<GameManager>();
                gameManager.GameOverCaller();
                DestroySequence();
            }
        }

        public void DestroySequence()
        {
            Transform tempClones = GameObject.Find("TempClones").transform;
            Instantiate(enemyDeathPrefab, transform.position, Quaternion.identity, tempClones);
            Destroy(gameObject, 0.05f);
        }
    }
}
