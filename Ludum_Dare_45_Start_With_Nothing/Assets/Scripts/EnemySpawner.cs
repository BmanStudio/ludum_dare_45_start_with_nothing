﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace Game.Enemis
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] Enemy enemyPrefab = null;
        [SerializeField] Transform tempClones = null;

        [SerializeField] float minX = -9f;
        [SerializeField] float maxX = 4f;

        [SerializeField] float restTimeBetweenWaves = 3f;
        [SerializeField] float initialSpawnCD = 1.5f;
        [SerializeField] int enemiesPerWave = 15;
        [SerializeField] float spawnCDInterval = 0.9f;

        [SerializeField] TextMeshProUGUI newWaveText;
        [SerializeField] TextMeshProUGUI waveCounter;
        private float timeSinceLastSpawn = 0;
        private bool isStopSpawn = false;
        private int enemiesSpawned;
        private float spawnCDByWave;

        private float currentWave;

        private void Start()
        {
            enemiesSpawned = 0;
        }

        void Update()
        {
            if (!isStopSpawn)
            {
                timeSinceLastSpawn += Time.deltaTime;
                StartCoroutine(CalculateWave());
                if (timeSinceLastSpawn > spawnCDByWave)
                {
                    SpawnEnemy();
                }
            }
        }

        private void SpawnEnemy()
        {
            Instantiate(enemyPrefab, GetRandomPosition(), Quaternion.identity, tempClones);
            timeSinceLastSpawn = 0;
            enemiesSpawned += 1;
        }

        private Vector3 GetRandomPosition()
        {
            float randomX = UnityEngine.Random.Range(minX, maxX);
            return new Vector3(randomX, transform.position.y, 0);
        }

        private IEnumerator CalculateWave()
        {
            if (currentWave == 0)
            {
                currentWave = 1;
                spawnCDByWave = initialSpawnCD;
            }
            if (enemiesSpawned / enemiesPerWave >= currentWave)
            {
                ToggleStopSpawn(true);
                CalculateNextWave();
                ShowWaveText(true);
                yield return new WaitForSeconds(restTimeBetweenWaves);
                ShowWaveText(false);
                ToggleStopSpawn(false);
            }
        }

        private void ShowWaveText(bool toggle)
        {
            newWaveText.gameObject.SetActive(toggle);
            newWaveText.text = "Wave " + currentWave + " Begins Soon..";
            waveCounter.text = "Wave: " + currentWave;
        }

        private void CalculateNextWave()
        {
            currentWave += 1;
            if (currentWave >= 4f)
            {
                enemiesPerWave += (int)(currentWave * 1.5f);

            }
            if (currentWave >= 7f)
            {
                restTimeBetweenWaves += 1.5f;
            }
            if (currentWave >= 10 && currentWave % 2 == 0)
            {
                spawnCDInterval -= 0.1f;
            }
            spawnCDByWave *= spawnCDInterval;
        }

        private void ToggleStopSpawn(bool toggle)
        {
            isStopSpawn = toggle;
        }
        private static void ExplodeAllEnemies()
        {
            Enemy[] enemies = FindObjectsOfType<Enemy>();
            foreach (Enemy enemy in enemies)
            {
                enemy.DestroySequence();
            }
        }
        public void GameOverSequence()
        {
            ToggleStopSpawn(true);
            ExplodeAllEnemies();
        }
    }
}
