﻿using Game.Nothing;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


namespace Game.Ui
{
    public class BankAmountDisplayer : MonoBehaviour
    {
        TextMeshProUGUI text;
        NothingBank nothingBank;

        private void Start()
        {
            nothingBank = FindObjectOfType<NothingBank>();
            text = GetComponent<TextMeshProUGUI>();
        }
        void Update()
        {
            text.text = nothingBank.AmountInBank() + " / " + nothingBank.GetBankCapacity();
        }
    }
}
