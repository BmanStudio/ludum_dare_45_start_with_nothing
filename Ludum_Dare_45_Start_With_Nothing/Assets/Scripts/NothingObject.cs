﻿using UnityEngine;

namespace Game.Nothing
{
    public class NothingObject : MonoBehaviour
    {
        [SerializeField] float inBankGravityScale = 1f;
        [SerializeField] float speedMoveInsideBank = 0.1f;
        [Range(50, 350)] [SerializeField] float rotationSpeed = 100f;

        private float lastTargetSpeed = 2f;

        private bool hasShooted = false;
        public bool HasShooted // find ref with this one!
        {
            get { return hasShooted; }
            set { hasShooted = value; }
        }

        private GameObject master;
        public bool IsAtBank { get; set; }
        private NothingBank nothingBank;
        SpriteRenderer spriteRenderer;

        private void OnEnable()
        {
            nothingBank = FindObjectOfType<NothingBank>();
            spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        }

        void Update()
        {
            if (master != null)
            {
                MoveToTarget(master, lastTargetSpeed);
            }

            if (hasShooted)
            {
                MoveInDirection(Vector3.up, lastTargetSpeed);
            }

            if (master == nothingBank.gameObject)
            {
                if (GetIsAtTargetPosition(master.transform.position))
                {
                    IsAtBank = true;
                    master = null;
                    TogglePhysics(false, inBankGravityScale, nothingBank.GetNothingSizeByAmount());
                }
            }
            if (IsAtBank)
            {
                UpdateScale(nothingBank.GetNothingSizeByAmount());
                MoveInDirection(MoveInRandomDirection(), speedMoveInsideBank);
            }
        }

        private void FixedUpdate()
        {
            transform.Rotate(Vector3.forward, rotationSpeed * Time.deltaTime);
        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!hasShooted) { return; }
            if (collision.gameObject.tag == "TriggerBorder")
            {
                Destroy(gameObject);
            }

            if (collision.gameObject.tag == "Enemy")
            {
                Destroy(gameObject);
            }
        }

        public void MoveToTarget(GameObject target, float targetSpeed)
        {
            if (master != target)
            {
                master = target;
            }

            if (!GetIsAtTargetPosition(target.transform.position))
            {
                lastTargetSpeed = targetSpeed;
                transform.position = Vector3.MoveTowards(transform.position, target.transform.position, targetSpeed);
            }
        }

        public void MoveInDirection(Vector3 direction, float targetSpeed)
        {
            master = null;
            lastTargetSpeed = targetSpeed;
            transform.position += direction * lastTargetSpeed * Time.deltaTime;
        }

        public bool GetIsAtTargetPosition(Vector3 targetPosition)
        {
            return transform.position == targetPosition;
        }

        public void TogglePhysics(bool toggle, float gravityScale, Vector3 newScale)
        {
            CircleCollider2D circleCollider = GetComponent<CircleCollider2D>();
            Rigidbody2D rigidbody = GetComponent<Rigidbody2D>();
            circleCollider.isTrigger = toggle;
            rigidbody.gravityScale = gravityScale;

            UpdateScale(newScale);

        }

        private void UpdateScale(Vector3 newScale) // TODO the scale changing causing a bug with makes the nothing to sometime no be shooted..
        {
            transform.localScale = newScale;
        }

        private Vector3 MoveInRandomDirection()
        {
            float randomX = Random.Range(0, 10);
            float randomY = Random.Range(0, 10);
            return new Vector3(randomX, randomY);
        }

        public void ChangeSpriteOrder(int order)
        {
            if (spriteRenderer == null) { print("is null"); return; }
            spriteRenderer.sortingOrder = order;
        }
    }
}
