﻿using Game.Nothing;
using UnityEngine;

namespace Game.Control
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] float speed = 15f;
        [SerializeField] NothingObject nothingPrefab = null;
        [SerializeField] Transform shootedParent = null;
        [SerializeField] float shootingCD = 1f;

        private bool hasMoved;
        private bool hasShot;
        public bool HasShot
        {
            get { return hasShot; }
            set { hasShot = value; }
        }
        private bool canMove;
        private Shooter shooter;

        private void Start()
        {
            shooter = FindObjectOfType<Shooter>();
        }
        void Update()
        {
            hasMoved = false;
            hasShot = false; // get changed inside shooter.Shoot;

            if (Input.GetKey(KeyCode.Space))
            {
                shooter.Shoot();
            }

            float moveHorizontal = Input.GetAxis("Horizontal");
            if (moveHorizontal != 0)
            {
                if (!canMove) { return; }
                transform.position += new Vector3(moveHorizontal, 0, 0) * Time.deltaTime * speed;
                hasMoved = true;
            }
            canMove = true;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            canMove = false;
        }

        public bool GetHasMoved()
        {
            return hasMoved;
        }

        public bool GetHasShot()
        {
            return hasShot;
        }
    }
}
