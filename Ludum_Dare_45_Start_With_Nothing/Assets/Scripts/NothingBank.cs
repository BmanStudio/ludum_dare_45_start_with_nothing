﻿using Game.Resources;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Nothing
{
    public class NothingBank : MonoBehaviour
    {
        [SerializeField] int startingNothing = 100;
        [SerializeField] int bankCapacity = 100;
        [SerializeField] NothingObject nothingPrefab = null;
        GameObject nothingSpawner;
        [SerializeField] float nothingSpeed = 0.2f;
        [SerializeField] float createCD = 0.2f;

        [SerializeField] float minNothingSizeInBank = 0.3f;

        [SerializeField] float MaxNothingSizeInBank = 3f;

        [SerializeField] UnityEvent spawnedNothing;

        private int nothingInBank;

        private NothingObject[] nothingArray;
        private float timeSinceLastSpawnNothing = 0;
        private float bankSizeX = 6f;
        private float bankSizeY = 3.2f;

        NothingBar nothingBar;

        void Start()
        {
            nothingSpawner = GameObject.FindObjectOfType<NothingSpawner>().gameObject;
            nothingBar = GameObject.FindObjectOfType<NothingBar>();
            nothingArray = new NothingObject[bankCapacity];
            StartCoroutine(CreateStartingNothing());
        }


        void Update()
        {
            timeSinceLastSpawnNothing += Time.deltaTime;
            if (nothingBar.GetBarIsEmpty())
            {
                if (GetBankIsFull()) { return; }
                CreateNothing();
            }

        }


        private IEnumerator CreateStartingNothing()
        {
            while (nothingInBank < startingNothing)
            {
                CreateNothing();
                yield return new WaitForSeconds(createCD);
            }
        }

        private void CreateNothing()
        {
            if (timeSinceLastSpawnNothing < createCD) { return; }
            if (nothingInBank < bankCapacity)
            {
                NothingObject newNothing = Instantiate(nothingPrefab, nothingSpawner.transform.position, Quaternion.identity, transform);
                spawnedNothing.Invoke();
                nothingArray[nothingInBank] = newNothing;
                nothingInBank += 1;
                newNothing.MoveToTarget(gameObject, nothingSpeed);
                newNothing.ChangeSpriteOrder(nothingInBank);
                timeSinceLastSpawnNothing = 0;
            }
        }


        public NothingObject DrawNothing()
        {
            if (nothingInBank > 0)
            {
                nothingInBank -= 1;
                nothingArray[nothingInBank].ChangeSpriteOrder(20);
                nothingArray[nothingInBank].IsAtBank = false;
                return nothingArray[nothingInBank];
            }
            else
            {
                return null;
            }
        }

        public bool GetBankIsFull()
        {
            return nothingInBank == bankCapacity;
        }

        public int AmountInBank()
        {
            return nothingInBank;
        }

        public int GetBankCapacity()
        {
            return bankCapacity;
        }

        public Vector3 GetNothingSizeByAmount()
        {
            float bankSize = bankSizeX * bankSizeY;
            float sizePerOne = bankSize / nothingInBank;
            float fixedScalse = Mathf.Clamp(sizePerOne, minNothingSizeInBank, MaxNothingSizeInBank);
            return new Vector3(fixedScalse, fixedScalse, fixedScalse);
        }
    }

}
