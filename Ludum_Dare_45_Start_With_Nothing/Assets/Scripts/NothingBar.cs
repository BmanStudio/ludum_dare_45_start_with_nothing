﻿using Game.Control;
using Game.Nothing;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Resources
{
    public class NothingBar : MonoBehaviour
    {
        [SerializeField] Image foreground = null;
        [SerializeField] float initialProductionSpeed = 2f;
        [SerializeField] float movementUsageSpeed = 0.2f;
        [SerializeField] float shootingUsageSpeed = 10f;

        private float nothingProgress = 1;

        private bool barIsEmpty;
        private bool hasMoved;
        private bool hasShot;
        GameObject player;
        PlayerController playerController;
        NothingBank nothingBank;
        void Start()
        {
            player = GameObject.FindWithTag("Player");
            playerController = player.GetComponent<PlayerController>();
            nothingBank = FindObjectOfType<NothingBank>();
        }

        void Update()
        {
            if (barIsEmpty)
            {
                if (nothingBank.GetBankIsFull()) { return; }
                nothingProgress = 1;
            }
            CalculateNothingness();

            DisplayTheBarUI();

        }

        private void DisplayTheBarUI()
        {
            foreground.rectTransform.localScale = new Vector3(nothingProgress, 1, 1);
        }

        private void CalculateNothingness()
        {
            hasMoved = playerController.GetHasMoved();
            hasShot = playerController.GetHasShot();
            if (nothingProgress > 0) // TODO Change 0 to math approximatally.
            {
                barIsEmpty = false;
                if (hasMoved || hasShot)
                {
                    /*if (Mathf.Approximately(nothingProgress, 1) == false)*/
                    if (nothingProgress < 1)
                    {
                        if (hasMoved)
                        {
                            nothingProgress = Mathf.Clamp(nothingProgress += movementUsageSpeed * (Time.deltaTime), 0, 1);
                        }
                        if (hasShot)
                        {
                            nothingProgress = Mathf.Clamp(nothingProgress += shootingUsageSpeed * (Time.deltaTime), 0, 1);
                        }
                    }
                }
                else
                {
                    nothingProgress = Mathf.Clamp(nothingProgress -= initialProductionSpeed * (Time.deltaTime), 0, 1);
                }
            }
            else
            {
                barIsEmpty = true;
            }
        }

        public bool GetBarIsEmpty()
        {
            return barIsEmpty;
        }


    }
}

