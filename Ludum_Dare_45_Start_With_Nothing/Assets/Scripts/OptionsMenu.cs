﻿using UnityEngine;
using UnityEngine.Audio;

namespace Game.Manager
{
    public class OptionsMenu : MonoBehaviour
    {
        [Header("Audio")]
        [SerializeField] AudioMixer audioMixer;
        float currentVolume = 0f;

        private int width;
        private int height;

        public void VolumeBySlider(float slider)
        {
            audioMixer.SetFloat("MasterVolume", slider);
        }

        public void ToggleFullScreen(bool fullScreen)
        {
            Screen.fullScreen = fullScreen;
        }

        public void setWidth(int newWidth)
        {
            width = newWidth;
        }

        public void setHeight(int newHeight)
        {
            height = newHeight;
        }
        public void ResolutionByButtons()
        {
            if (width == Screen.currentResolution.width) { return; }
            Screen.SetResolution(width, height, Screen.fullScreen);
        }


        public void DefaultValues()
        {
            audioMixer.SetFloat("MasterVolume", currentVolume);
        }


    }
}
