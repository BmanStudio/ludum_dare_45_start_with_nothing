﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game.Manager
{
    public class MainMenuManager : MonoBehaviour
    {
        [Header("Main Menu")]
        [SerializeField] GameObject buttons;
        [SerializeField] Slider slider;

        [Header("Options Menu")]
        [SerializeField] GameObject optionsMenu;


        [SerializeField] GameObject howToWindow;
        public void LoadGame()
        {
            buttons.SetActive(false);
            StartCoroutine(LoadingProgression());
        }

        public void OpenOptionsMenu()
        {
            optionsMenu.SetActive(true);
            buttons.SetActive(false);
        }

        public void CloseOptionsMenu()
        {
            optionsMenu.SetActive(false);
            buttons.SetActive(true);
        }

        public void OpenHowToWindow()
        {
            howToWindow.SetActive(true);
            buttons.SetActive(false);
        }

        public void CloseHowToWindow()
        {
            howToWindow.SetActive(false);
            buttons.SetActive(true);
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        private IEnumerator LoadingProgression()
        {
            AsyncOperation operation = SceneManager.LoadSceneAsync(1);
            slider.gameObject.SetActive(true);
            while (!operation.isDone)
            {
                float progress = Mathf.Clamp01(operation.progress / 0.9f);
                slider.value = progress;
                yield return null;
            }
        }
    }
}
