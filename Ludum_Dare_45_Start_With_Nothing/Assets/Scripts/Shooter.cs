﻿using Game.Control;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Nothing
{
    public class Shooter : MonoBehaviour
    {
        [SerializeField] float shootingCD = 1f;
        [SerializeField] float maxYcoordinate = 10f;
        [SerializeField] float nothingDrawSpeed = 2f;
        [SerializeField] float shootSpeed = 1f;
        [SerializeField] float maxDistanceFromPlayer = 0.2f;
        [SerializeField] Transform tempClones;

        [SerializeField] UnityEvent onShoot;

        private float timeSinceLastShot = 0f;
        private NothingBank nothingBank;
        private NothingObject nothing = null;
        private bool nothingObjectInPossession = false;
        PlayerController playerController;

        private Vector3 defaultNothingScale = new Vector3(1, 1, 1);

        void Start()
        {
            playerController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
            nothingBank = FindObjectOfType<NothingBank>();
        }

        void Update()
        {
            if (!nothingObjectInPossession)
            {
                DrawNothingFromBank();
            }
            timeSinceLastShot += Time.deltaTime;
        }
        public void Shoot()
        {
            if (timeSinceLastShot < shootingCD) { return; }
            if (!nothingObjectInPossession) { return; }
            if (NothingIsAtShooterPosition())
            {
                onShoot.Invoke();
                nothing.HasShooted = true;
                playerController.HasShot = true;
                nothing.MoveInDirection(Vector3.up, shootSpeed);
                nothing.ChangeSpriteOrder(100);
                nothing = null;
                nothingObjectInPossession = false;
                timeSinceLastShot = 0;
            }
        }

        private bool NothingIsAtShooterPosition()
        {
            if (nothing == null)
            {
                return false;
            }

            var distance = Vector3.Distance(transform.position, nothing.transform.position);
            bool isIt = distance <= maxDistanceFromPlayer;
            return isIt;
        }

        private void DrawNothingFromBank()
        {
            nothing = nothingBank.DrawNothing();
            if (nothing != null)
            {
                nothing.transform.parent = tempClones;
                nothing.MoveToTarget(gameObject, nothingDrawSpeed);
                nothingObjectInPossession = true;
                nothing.TogglePhysics(true, 0f, defaultNothingScale); // apply triggring on the nothingobject
            }
        }
    }
}
