﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game.Manager
{
    public class GameManager : MonoBehaviour
    {
        [Header("Puase Menu")]
        [SerializeField] GameObject pauseMenuCanvas;

        [Header("Game Over Menu")]
        [SerializeField] GameObject gameOverCanvas;
        [SerializeField] Slider slider;
        [SerializeField] GameObject gameOverButtons;

        [SerializeField] UnityEvent gameOverEvent;


        public static bool isPaused = false;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
            {
                if (isPaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
            if (Debug.isDebugBuild)
            {
                if (Input.GetKeyDown(KeyCode.T))
                {
                    print(Time.timeScale);
                }
            }
        }

        public void Pause()
        {
            pauseMenuCanvas.SetActive(true);
            Time.timeScale = 0f;
            isPaused = true;
        }

        public void Resume()
        {
            pauseMenuCanvas.SetActive(false);
            Time.timeScale = 1f;
            isPaused = false;
        }

        public void LoadMainMenu()
        {
            Resume();
            SceneManager.LoadScene(0);
        }


        public void GameOverCaller()
        {
            gameOverEvent.Invoke();
            gameOverCanvas.SetActive(true);
            Time.timeScale = 0f;
        }


        public void ReloadLevel()
        {
            gameOverButtons.SetActive(false);
            Resume();
            StartCoroutine(LoadingProgression());
        }

        private IEnumerator LoadingProgression()
        {
            AsyncOperation operation = SceneManager.LoadSceneAsync(1);
            slider.gameObject.SetActive(true);
            while (!operation.isDone)
            {
                float progress = Mathf.Clamp01(operation.progress / 0.9f);
                slider.value = progress;
                yield return null;
            }
        }

    }
}
