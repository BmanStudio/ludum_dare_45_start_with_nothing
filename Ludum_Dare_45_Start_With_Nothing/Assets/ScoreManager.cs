﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Game.manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] float scoringSpeed = 10f;
        [SerializeField] TextMeshProUGUI scoreText;
        [SerializeField] TextMeshProUGUI highScoreText;


        private float currentScore;
        private int fixedScore;
        private int currentHighScore;
        // Start is called before the first frame update
        void Start()
        {
            currentScore = 0;
            if (!PlayerPrefs.HasKey("HighScore"))
            {
                PlayerPrefs.SetInt("HighScore", 0);
                currentHighScore = PlayerPrefs.GetInt("HighScore");
            }
            else
            {
                currentHighScore = PlayerPrefs.GetInt("HighScore");
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (scoreText != null)
            {
                currentScore += scoringSpeed * Time.deltaTime;
                fixedScore = (int)(currentScore);
                SetHighScore();
                scoreText.text = "Score: " + fixedScore.ToString();
                highScoreText.text = "HighScore: " + currentHighScore.ToString();
            }
            else
            {
                highScoreText.text = currentHighScore.ToString();
            }
        }

        public void SetHighScore()
        {
            if (fixedScore > PlayerPrefs.GetInt("HighScore"))
            {
                PlayerPrefs.SetInt("HighScore", fixedScore);
                currentHighScore = PlayerPrefs.GetInt("HighScore");
            }
        }
    }
}
