﻿using UnityEngine;

public class selfDestroy : MonoBehaviour
{
    [SerializeField] float timer = 1f;
    void Start()
    {
        Destroy(gameObject, timer);
    }
}
