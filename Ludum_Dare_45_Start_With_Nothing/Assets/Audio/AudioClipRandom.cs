﻿using UnityEngine;

public class AudioClipRandom : MonoBehaviour
{
    [SerializeField] AudioClip[] audioClips = null;
    AudioSource audioSource = null;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void RandomSoundFromList()
    {
        if (audioSource.isPlaying) { return; }
        audioSource.clip = audioClips[Random.Range(0, audioClips.Length)];
        audioSource.Play();
    }
}
